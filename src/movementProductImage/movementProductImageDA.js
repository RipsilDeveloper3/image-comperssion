var request = require('request');
var Product = require('./../model/product.model');
var s3 = require('./../config/s3.config');

var Imagemin = require('imagemin');
var imageminWebp = require('imagemin-webp');
var imageminJpeg = require('imagemin-jpegtran');
var env = require('./../config/s3.env');
var AWS = require('aws-sdk');

// get reference to S3 client
exports.uploadImageDropboxToS3 = function (req, res) {
  /* var bulk = Product.collection.initializeOrderedBulkOp(); */
  /* bulk.find( { status: "D" }).find();
  bulk.execute();
  bulk.getOperations(); */
/* 
  Product.find({}).select('_id productImage child').exec(function (err, findImageData) {
    if (err) {
      res.status(500).send({
        "result": 'error occured while retreiving data'
      });
    } else {
      findImageData.forEach(e => {
        e.productImage.forEach(imageName => {
          const url = imageName.productImageName;
          request.get({ url: url, encoding: null }, (err, dropBoxRes, body) => {
            if (!err) {
              const type = dropBoxRes.headers["content-type"];
              const imageDescription = dropBoxRes.headers['content-disposition'];
              var imageName = imageDescription.match(/filename="(.+)"/)[1];
              const prefix = "data:" + type + ";base64,";
              const base64 = body.toString('base64');
              const dataUri = prefix + base64;
              const base64Data = Buffer.from(dataUri.replace(/^data:image\/\w+;base64,/, ""), 'base64');
              const params = {
                Bucket: env.CatalogueBucket + '/' + 'product' + '/' + 'original' + '/' + e._id, // create a folder and save the image
                Key: imageName,
                ACL: 'public-read',
                ContentEncoding: 'base64',
                Body: base64Data,
                ContentType: type
              };
              s3.upload(params, function (err, data) {
                if (err) {
                  console.log(err);
                } else {
                 console.log(params);
                }
              });
            }
          });
        })
      });

      bulk.execute(function (err, result) {
        // do something with the result here
        if (err) {
      console.log(err)
        } else {
          console.log(result);
        }
      });


    }
  });
 */

  // note : value may be undefined
  
  
  SOURCE_BUCKET = 'students-bus-catalogue-images/images/test/1';
  key= 'HYSS0149 (2).JPG';
  s3.getObject({ Bucket: SOURCE_BUCKET, Key: key }, function(err, data) {
    if (err) {
      res.status(500).send({
        "result": err
      });
    } else {
    /*   
      s3.putObject({
        Bucket: 'students-bus-catalogue-images/images/test/1',
        Key: 'HYSS0149 (2)T.JPG',
        Body: data.Body,
        ContentType: 'image/webp'
    }, function(err, putData){
        if(err){
          console.log(err);
        } else {
          res.status(200).send(putData);
        }
    }); */
      
    
      var imageCompressor = Imagemin.buffer(data.Body, {
        plugins: [
          imageminWebp({quality: 50})
        ]
        });
        console.log(imageCompressor);
        
        
        Promise.resolve(imageCompressor).then(
          resolved => {
            
        /*     s3.putObject({
              Bucket: 'students-bus-catalogue-images/images/test/1',
              Key: 'HYSS0149 (2)T.JPG',
              Body: base64Data,
              ContentType: 'image/webp'
          }, function(err, putData){
              if(err){
                console.log(err);
              } else {
                res.status(200).send(putData);
              }
          }); */
          res.status(200).send(resolved);
          },
          rejected => {
            console.log("Error:");
            console.log(rejected);
          });
    }
});
}
   
