var movementProductImageDA = require('./movementProductImageDA');

exports.uploadImageDropboxToS3 = function (req, res) {
    try {
        movementProductImageDA.uploadImageDropboxToS3(req, res);
    } catch (error) {
      console.log(error);
    }
  }
  