'use strict';

var movementProductImageMgr = require('./movementProductImageMgr');


module.exports = function (app) {
    
    app.route('/moveproduct')
    .put(movementProductImageMgr.uploadImageDropboxToS3);
}
