var imagesRoutes = require('./images/imagesRoute');
var contentMgmtRoutes = require('./content-mgmt/contentMgmtRoute');
var productRoutes = require('./product/productRoute');
var mvpRoute = require('./mvp/mvpRoute');
var movementProductRoutes = require('./movementProductImage/movementProductRoute');

exports.loadRoutes = function (app) {
  
  imagesRoutes(app);
  productRoutes(app);
  contentMgmtRoutes(app);
  mvpRoute(app);
  movementProductRoutes(app);
};
