var s3 = require('../../config/s3.config');
var env = require('../../config/s3.env');
var request = require('request');
var https = require('https');
var fs = require('fs');

exports.uploadMeasurementImage = function (req, res) {
    const base64Data = Buffer.from(req.body.uploadedImage.replace(/^data:image\/\w+;base64,/, ""), 'base64');
    const type = req.body.uploadedImage.split(';')[0].split('/')[1]
    const params = {
      Bucket: env.ProductBucket + '/' + 'images' + '/' + 'measurement' + '/' + req.params.id, // create a folder and save the image
      Key: req.body.imageName,
      ACL: 'public-read',
      ContentEncoding: 'base64',
      Body: base64Data,
      ContentType: `image/${type}`
    };
  
    s3.upload(params, function (err, data) {
      if (err) {
        console.log(err);
      } else {
        res.status(200).json(data);
      }
    });
  }
  exports.getObjectBuffer = function(req, res) {
    const params = {
      Bucket: env.ProductBucket,
      Key: 'images' + '/' + 'measurement' + '/' + req.params.id + '/' + req.body.imageName,
    };
    s3.getObject(params, function(err, data) {
      if (err) {
        res.status(500).json(err);
      } else {
        var attachement, file;
        file = new Buffer(data.Body, 'binary');
        attachement = file.toString('base64');
        res.status(200).json(attachement);
      }
    })
  }

  exports.downloadDropBoxImage = function(req, res) {
    var link = req.body.link;
    var firstPart = link.split("?")[0];
    link = firstPart + '?dl=0';
   /*  var link = "https://www.dropbox.com/s/dr1cit55idwi1m0/english_buisness_message.txt?dl=0"
    var firstPart = link.split("=")[0];
    link = firstPart + '=1'; */
    var myFile = request(link).pipe(fs.createWriteStream('../../dist/'));
    myFile.write(resFull.data);
    myFile.end(() => {
        console.log(myFile);
     })
  //   /* var myFile = request(link).pipe(fs.createWriteStream('../../dist/image.jpeg')); */
  //   var file = fs.createWriteStream('../../dist/');

  //   var request = (link) => {
  //     https.get(link, (response) => {
  //       if (response.statusCode == 302) { // it's a redirect!
  //         request(response.headers.location);
  //       } else {
  //         response.pipe(fp);
  //         file.on('finish', () => {
  //           file.close(() => {
  //             // You may do something here
  //             res.status(200).json(myFile);
  //           });
  //         });
  //       }
  //     }).on('error', (err) => {
  //       // Do something if the request fails
  //     });

      
  // /*   myFile.write(resFull.data);
  //   myFile.end(() => {
  //       res.status(200).json(myFile);
  //    }) */
  // }
  // request(link);
  /* var file = fs.createWriteStream("../../dist/");
var request = https.get(link, function(response) {
  response.pipe(file);
  console.log(response);
}); */

}