'use strict';
var productImageMgr = require('./productimage/productImageMgr');
var categoryMgr = require('./categoryimage/categoryMgr');
var brandMgr = require('./brandimage/brandMgr');
var upload = require('../config/multer.config');
var sizeGuide = require('./productSizeGuide/productSizeGuideMgr');
var measurementImageMgr = require('./measurementImage/measurementImageMgr');
var howToMeasureMgr = require('./howToMeasure/howToMeasureMgr');

module.exports = function (app) {
  /* app.route('/imagesingle/:id')
    .put(productImageMgr.uploadSingleImage); */
  /* app.route('/productimage/:id')
    .put(upload.single('single'), productImageMgr.uploadSingleImageMulter);
    
 */
  app.route('/productimagesthree/:id')
    .put(upload.array('uploads[]'), productImageMgr.uploadMultiImageMulter);
  app.route('/editproductimagesthree/:id') // edit product image
    .put(upload.array("uploads[]"), productImageMgr.uploadMultiImageEdit);
    app.route('/brandimagesthree/:id')
    .put(brandMgr.uploadBrandImages); // Upload Brand Image
// app.route('/supercategoryimagesthree/:id')
//     .put(upload.single('single'), categoryMgr.uploadSuperCategoryImages); // Upload Super Category

//     app.route('/supercategorysthree/:supId/maincategoryimagesthree/:mainId')
//     .put(upload.single('single'), categoryMgr.addMainCategoryImage); // Upload Main 
    
    
//     app.route('/subcategoryimagesthree/:supId/add/:mainId/addValue/:subId')
//     .put(upload.single('single'), categoryMgr.addSubCategoryImage); // Upload Sub Category Image 
    
    app.route('/sizeimage/:id')
    .put(sizeGuide.uploadSizeGuideImages); // Upload Size Guide

    app.route('/uploadcateogorybanner/:id')
    .post(categoryMgr.uploadCategoryBannerImage); // Upload  Category Banner Image 

    app.route('/uploadmeasurementimage/:id')
    .post(measurementImageMgr.uploadMeasurementImage);

    app.route('/getobjectfromimage/:id')
    .post(measurementImageMgr.getObjectBuffer);
 /*    app.route('/uploadhowtomeasure/:id')
    .post(upload.single('single'), howToMeasureMgr.uploadImageHowToMeasure); */

    app.route('/base64howtomeasure/:id')
    .post(upload.single('single'), howToMeasureMgr.uploadImageHowToMeasure);
    app.route('/uploadhowtomeasure/:id')
    .post(upload.single('single'), howToMeasureMgr.uploadImageHowToMeasure);
    // Category
    app.route('/supercategoryimagesthree/:id')
    .put(categoryMgr.uploadSuperCategoryImage);
    app.route('/maincategoryimagesthree/:id')
    .put(categoryMgr.uploadMainCategoryImage);
    app.route('/subcategoryimagesthree/:id')
    .put(categoryMgr.uploadSubCategoryImage);
    app.route('/downloaddropboximage')
    .put(measurementImageMgr.downloadDropBoxImage);
}
