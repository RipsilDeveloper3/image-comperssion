var brandDA = require('./brandDA');
var s3 = require('../../config/s3.config');
var env = require('../../config/s3.env');


exports.uploadBrandImages = function (req, res) {
    try {
      brandDA.uploadBrandImages(req, res);
    } catch (error) {
      console.log(error);
    }
  }