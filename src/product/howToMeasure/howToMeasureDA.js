var s3 = require('../../config/s3.config');
var env = require('../../config/s3.env');
const AWS = require('aws-sdk');

// exports.uploadImageHowToMeasure = function (req, res) {
//     try {
//       const params = {
//         Bucket: env.ProductBucket + '/' + 'images' + '/' + 'howtomeasure' + '/' + req.params.id, // create a folder and save the image
//         Key: req.file.originalname,
//         Body: req.file.buffer,
//       };
//       s3.upload(params, function (err, data) {
//         if (err) {
//           console.log(err);
//         } else {
//            res.status(200).json(params);
//         }
//       });
//     } catch (error) {
//       console.log(error);
//     }
//     } 

    exports.uploadImageHowToMeasure = function (req, res) {
        const base64Data = Buffer.from(req.body.image.replace(/^data:image\/\w+;base64,/, ""), 'base64');
        const type = req.body.image.split(';')[0].split('/')[1]
        const params = {
          Bucket: env.ProductBucket + '/' + 'images' + '/' + 'howtomeasure' + '/' + req.params.id, // create a folder and save the image
          Key: req.body.image64,
          ACL: 'public-read',
          ContentEncoding: 'base64',
          Body: base64Data,
          ContentType: `image/${type}`
        };
      
        s3.upload(params, function (err, data) {
          if (err) {
            console.log(err);
          } else {
            res.status(200).json(data);
          }
        });
      }