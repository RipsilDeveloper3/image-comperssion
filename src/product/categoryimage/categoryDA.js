var SuperCategory = require('./../../model/superCategory.model');
var s3 = require('../../config/s3.config');
var env = require('../../config/s3.env');

/* exports.uploadSuperCategoryImages = function (req, file, res) {
  res.status(200).json(file);
}

exports.addMainCategoryImage = function (req, file, res) {
  res.status(200).json(file);
}

exports.addSubCategoryImage = function (req, file, res) {
  res.status(200).json(file);
} */

exports.uploadSuperCategoryImage = function (req, res) {
  const base64Data = Buffer.from(req.body.categoryImageName.replace(/^data:image\/\w+;base64,/, ""), 'base64');
  const type = req.body.categoryImageName.split(';')[0].split('/')[1]
  const params = {
    Bucket: env.ProductBucket + '/' + 'images' + '/' + 'category' + '/' + req.params.id, // create a folder and save the image
    Key: req.body.categoryImage,
    ACL: 'public-read',
    ContentEncoding: 'base64',
    Body: base64Data,
    ContentType: `image/${type}`
  };

  s3.upload(params, function (err, data) {
    if (err) {
      console.log(err);
    } else {
        console.log(data);
      res.status(200).json(data);
    }
  });
}
exports.uploadMainCategoryImage = function (req, res) {
  const base64Data = Buffer.from(req.body.categoryImageName.replace(/^data:image\/\w+;base64,/, ""), 'base64');
  const type = req.body.categoryImageName.split(';')[0].split('/')[1]
  const params = {
    Bucket: env.ProductBucket + '/' + 'images' + '/' + 'maincategory' + '/' + req.params.id, // create a folder and save the image
    Key: req.body.categoryImage,
    ACL: 'public-read',
    ContentEncoding: 'base64',
    Body: base64Data,
    ContentType: `image/${type}`
  };

  s3.upload(params, function (err, data) {
    if (err) {
      console.log(err);
    } else {
        console.log(data);
      res.status(200).json(data);
    }
  });
}
exports.uploadSubCategoryImage = function (req, res) {
  const base64Data = Buffer.from(req.body.categoryImageName.replace(/^data:image\/\w+;base64,/, ""), 'base64');
  const type = req.body.categoryImageName.split(';')[0].split('/')[1]
  const params = {
    Bucket: env.ProductBucket + '/' + 'images' + '/' + 'subcategory' + '/' + req.params.id, // create a folder and save the image
    Key: req.body.categoryImage,
    ACL: 'public-read',
    ContentEncoding: 'base64',
    Body: base64Data,
    ContentType: `image/${type}`
  };

  s3.upload(params, function (err, data) {
    if (err) {
      console.log(err);
    } else {
        console.log(data);
      res.status(200).json(data);
    }
  });
}
exports.uploadCategoryBannerImage = function (req, res) {
  const base64Data = Buffer.from(req.body.uploadedImage.replace(/^data:image\/\w+;base64,/, ""), 'base64');
  const type = req.body.uploadedImage.split(';')[0].split('/')[1]
  const params = {
    Bucket: env.ProductBucket + '/' + 'images' + '/' + 'categorybanner' + '/' + req.params.id, // create a folder and save the image
    Key: req.body.imageName,
    ACL: 'public-read',
    ContentEncoding: 'base64',
    Body: base64Data,
    ContentType: `image/${type}`
  };

  s3.upload(params, function (err, data) {
    if (err) {
      console.log(err);
    } else {
        console.log(data);
      res.status(200).json(data);
    }
  });
}