var s3 = require('../../config/s3.config');
var env = require('../../config/s3.env');
const AWS = require('aws-sdk');

/* var adsDetails = require('../../model/ads.model');
var env = require('../../config/s3.env');
const AWS = require('aws-sdk');

exports.uploadImageADS = function (req, file, res) {
    adsDetails.findOne({
        '_id': req.params.id
    }).select().exec(function (err, data) {
        if (err) {
            res.status(500).json(err);
        } else {
            data.adsImageName = file.Key;
            data.save(function (err, data) {
                if (err) {
                    res.status(500).json(err);
                } else {
                    res.status(200).json(data);
                }
            })
        }
    })
}
exports.updateADSImage = function (req, file, res) {
    adsDetails.findOne({
        '_id': req.params.id,
    }, function (err, data) {
        if (err) {
            res.status(500).json(err);
        } else {
            if (data.adsImageName === file.Key) {
                res.status(200).json(data);
            } else {
                var s3 = new AWS.S3();
                s3.deleteObject({
                    Bucket: env.homebucket,
                    Key: 'images' + '/' + 'ads' + '/' + req.params.id + '/' + data.adsImageName
                }, function (err, data) {
                    if (err) {
                        res.status(500).json(err);
                    } else {
                        adsDetails.findOne({
                            '_id': req.params.id
                        }).select().exec(function (err, data1) {
                            if (err) {
                                res.status(500).json(err);
                            } else {
                                data1.adsImageName = file.Key;
                                data1.save(function (err, data) {
                                    if (err) {
                                        res.status(500).json(err);
                                    } else {
                                        data.adsImageName = env.HomeImageServerPath + 'ads' + '/' + data._id + '/' + data.adsImageName;
                                        res.status(200).json(data);
                                    }
                                })
                            }
                        })
                    }
                })
            }

        }
    });
}
 */

 // base64 image upload

 exports.uploadAdsBaseSingleImage = function (req, res) {
    const base64Data = Buffer.from(req.body.adsImageName.replace(/^data:image\/\w+;base64,/, ""), 'base64');
    const type = req.body.adsImageName.split(';')[0].split('/')[1]
    const params = {
      Bucket: env.homebucket + '/' + 'images' + '/' + 'smallbanner' + '/' + req.params.id, // create a folder and save the image
      Key: req.body.adsName,
      ACL: 'public-read',
      ContentEncoding: 'base64',
      Body: base64Data,
      ContentType: `image/${type}`
    };
  
    s3.upload(params, function (err, data) {
      if (err) {
        console.log(err);
      } else {
        res.status(200).json(data);
      }
    });
  }