var mongoose = require('mongoose');
const PerformanceSchema = new mongoose.Schema({

target: String, // performance target
unconditionalDiscount: String,
tradingDiscount: String,
orderSize: String,
cashDiscount: String,
cashDiscountPaid: String,
promotionalProgram: String,
catalogueParticipation:String,
newLaunches: String,
themeParticipation: String,
specilalization: String,
liquidationOffer: String

});
module.exports = PerformanceSchema;