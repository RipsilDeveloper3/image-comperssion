var mongoose = require('mongoose');
var MainCategory = require('./mainCategory.model');

const SuperCategorySchema = new mongoose.Schema({
  categoryName: String,
  categoryDescription: String,
  mainCategory: [MainCategory],
  sortOrder: Number,
  status: String,
  categoryImageName: String
});

const Category = mongoose.model('category', SuperCategorySchema);
module.exports = Category;