var mongoose = require('mongoose');
var DeliveryModel = require('./deliveryDetails.model');
var OfficeAddress = require('./vendorOfficeAddressDetails.model');
var SupplyLocationAddress = require('./vendorWareHouseAddressDetail.model');
var PaymentModel = require('./paymentDetials.model');
var PerformanceModel = require('./performanceDetails.model');
var SignatureModel = require('./signatureDetails.model');

const vendorsDetailsSchema = new mongoose.Schema({
    vendorName: String,
    vendorEmailId: String,
    vendorMobileNumber: String,
    password: String,
    vendorType: String,
    vendorContactPerson: String,
    vendorContactPersonMobileNumber: String,
    vendorContactPersonDesignation: String,
    officeAddressDetails: [OfficeAddress],
    supplyLocationAddressDetails: [SupplyLocationAddress],
    merchandiseDivision: String,
    companyStatus: String,
    contractNumber: String,
    contractDate: Date,
    contractStartDate: Date,
    contractEndDate: Date,
    paymentDetails: [PaymentModel],
    deliveryDetails: [DeliveryModel],
    performanceDetails: [PerformanceModel],
    signatureDetails: [SignatureModel],
    digitalSignature: String,
    cancelledCheque: String,
    vendorCode: String

});
const vendorDetail = mongoose.model('vendorAccount', vendorsDetailsSchema);
module.exports = vendorDetail;