var imagesDA = require('./imagesDA');
var s3 = require('../config/s3.config');
var env = require('../config/s3.env');
const AWS = require('aws-sdk');

exports.uploadSingleImage = function (req, res) {
  try {
    imagesDA.uploadSingleImage(req, res);
  } catch (error) {
    console.log(error);
  }
}


exports.uploadMultiImageMulter = function (req, res) {
  var promises = [];
  for (var i = 0; i < req.files.length; i++) {
    var file = req.files[i];
    promises.push(uploadLoadToS3(req, file));
  }
  Promise.all(promises).then(function (data) {
    imagesDA.uploadMultiImageMulter(req, data, res);
  }).catch(function (err) {
    console.log(err);
  });

  function uploadLoadToS3(req, item) {
    const params = {
      Bucket: env.Bucket + '/' + 'images' + '/' + 'brand' + '/' + req.params.id, // create a folder and save the image
      Key: item.originalname,
      Body: item.buffer,
    }
    return s3.upload(params).promise();
  }
} 

exports.uploadSingleImageMulter = function (req, res) {
  try {
    const params = {
      Bucket: env.Bucket + '/' + 'images' + '/' + 'brand' + '/' + req.params.id, // create a folder and save the image
      Key: req.file.originalname,
      ACL: 'public-read',
      Body: req.file.buffer,
    };

    s3.upload(params, function (err, data) {
      if (err) {
        console.log(err);
      } else {
        imagesDA.uploadSingleImageMulter(req, data, res);
      }
    });
  } catch (error) {
    console.log(error);
  }
}