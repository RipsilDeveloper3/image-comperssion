var s3 = require('../config/s3.config');
var env = require('../config/s3.env');
const AWS = require('aws-sdk');



exports.uploadSingleImage = function (req, res) {
  const base64Data = Buffer.from(req.body.brandImageName.replace(/^data:image\/\w+;base64,/, ""), 'base64');
  const type = req.body.brandImageName.split(';')[0].split('/')[1]
  const params = {
    Bucket: env.Bucket + '/' + 'images' + '/' + 'brand' + '/' + req.params.id, // create a folder and save the image
    Key: req.body.brandName,
    ACL: 'public-read',
    ContentEncoding: 'base64',
    Body: base64Data,
    ContentType: `image/${type}`
  };

  s3.upload(params, function (err, data) {
    if (err) {
      console.log(err);
    } else {
        console.log(data);
      res.status(200).json(data);
    }
  });
}

exports.uploadSingleImageMulter = function (req, file, res) {
    console.log(file)
    res.status(200).json(file);
  
}


exports.uploadMultiImageMulter = function (req, file, res) {
    console.log(file)
    res.status(200).json(file);
}