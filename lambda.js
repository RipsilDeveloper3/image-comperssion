'use strict'
const awsServerlessExpress = require('aws-serverless-express');
const app = require('./src/server');
const binaryMimeTypes = [
	'application/octet-stream',
	'font/eot',
	'font/opentype',
	'font/otf',
	'image/jpeg',
	'image/png',
	'image/svg+xml'
];

const server = awsServerlessExpress.createServer(app, null, binaryMimeTypes);
exports.handler = (event, context) => { 
	
	process.env['PATH'] = process.env['PATH'] + ':' + process.env['LAMBDA_TASK_ROOT'] + ':' + '/tmp/';
	console.log(process.env['PATH']);
	awsServerlessExpress.proxy(server, event, context)};
